import express from 'express';
import axios from 'axios';
import cors from 'cors';
import dotenv from 'dotenv';
import bodyParser from 'body-parser';
import fs from 'fs';
import path from 'path';
import { v4 as uuidv4 } from 'uuid';
import { fileURLToPath } from 'url';

dotenv.config();

const app = express();
const PORT = process.env.PORT || 3001;

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

app.use(cors());
app.use(bodyParser.json({ limit: '10mb' }));
app.use(express.json());

app.get('/api/spotify-callback', async (req, res) => {
  const { code } = req.query;
  const { SPOTIFY_CLIENT_ID, SPOTIFY_CLIENT_SECRET, REDIRECT_URI } = process.env;
  const authBuffer = Buffer.from(`${SPOTIFY_CLIENT_ID}:${SPOTIFY_CLIENT_SECRET}`).toString('base64');

  try {
    const response = await axios.post('https://accounts.spotify.com/api/token', null, {
      headers: {
        'Authorization': `Basic ${authBuffer}`,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      params: {
        grant_type: 'authorization_code',
        code,
        redirect_uri: REDIRECT_URI,
      },
    });

    const accessToken = response.data.access_token;
    res.redirect(`http://localhost:5173/home?access_token=${accessToken}`);
  } catch (error) {
    console.error('Error exchanging code for token:', error.response ? error.response.data : error.message);
    res.status(500).json({ error: 'Failed to exchange code for token' });
  }
});

app.get('/api/top-artists', async (req, res) => {
  const { accessToken } = req.query;

  try {
    const response = await axios.get('https://api.spotify.com/v1/me/top/artists', {
      headers: {
        'Authorization': `Bearer ${accessToken}`,
      },
      params: {
        time_range: 'medium_term',
        limit: 10,
      },
    });

    res.json(response.data.items.map(artist => artist.name));
  } catch (error) {
    console.error('Error fetching top artists:', error.response ? error.response.data : error.message);
    res.status(500).json({ error: 'Failed to fetch top artists' });
  }
});

app.get('/api/artist-country', async (req, res) => {
  const { artistName } = req.query;

  try {
    const searchResponse = await axios.get('https://musicbrainz.org/ws/2/artist', {
      params: {
        query: `artist:${artistName}`,
        fmt: 'json',
      },
    });

    const artists = searchResponse.data.artists;
    if (!artists || artists.length === 0) {
      return res.status(404).json({ error: `Artist '${artistName}' not found on MusicBrainz.` });
    }

    const mbid = artists[0].id;
    const artistResponse = await axios.get(`https://musicbrainz.org/ws/2/artist/${mbid}`, {
      params: {
        fmt: 'json',
      },
    });

    const country = artistResponse.data.country;
    res.json({ name: artistName, country });
  } catch (error) {
    console.error(`Error fetching data for artist '${artistName}' from MusicBrainz:`, error.response ? error.response.data : error.message);
    res.status(500).json({ error: 'Failed to fetch artist country' });
  }
});

app.post('/save-image', (req, res) => {
  const { imageData } = req.body;
  const imageId = uuidv4();
  const filePath = path.join(__dirname, 'images', `${imageId}.png`);

  const base64Data = imageData.replace(/^data:image\/png;base64,/, "");

  fs.writeFile(filePath, base64Data, 'base64', (err) => {
    if (err) {
      console.error(err);
      return res.status(500).send('Error saving image');
    }
    const imageUrl = `${req.protocol}://${req.get('host')}/images/${imageId}.png`;
    res.send({ url: imageUrl });
  });
});

app.use('/images', express.static(path.join(__dirname, 'images')));

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
